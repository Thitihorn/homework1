package day1;

public class lab2 {

	public static void bark(String a) {
		System.out.println("The dog name = " + a);
	}

	public float floattoInt(float x) {
		System.out.print("a.) get float = " + String.format("%f", x) );
		int ft = (int) x;
		System.out.println(" convert to int = " + ft);
		return x;
	}
	
	public int intoFloat(int x) {
		System.out.print("b.) get int = " + x);
		float intofloat = (float) x;
		System.out.println(" convert to float = " + String.format("%f", intofloat) );
		return x;
	}

	public double dbtofloat(double x) {
		System.out.print("c.) get double = " + x);
		float ft = (float) x;
		System.out.println(" convert to float = " + String.format("%f", ft));
		return x;
	}

	public char chtoInt(char x) {
		System.out.print("d.) get char = " + x);
		x = (char)x;
		int a = x;
		System.out.println(" convert to integer = " + a);
		return x;
	}

	public static void main(String[] args) {
		lab2 lb2 = new lab2();
		String dogname = "Mimi";
		lab2.bark(dogname);

		lb2.floattoInt(3.1F);
		lb2.intoFloat(2147483647);
		lb2.dbtofloat(1.234567890123);
		lb2.chtoInt('A');

		final String hello = "HELLO";
		System.out.println(hello);
		/*
		 * hello = "WORDL"; ==> Exception in thread "main" java.lang.Error: Unresolved
		 * compilation problem: The final local variable hello cannot be assigned. It
		 * must be blank and not using a compound assignment
		 * 
		 * System.out.println(hello);
		 */
	}
}
