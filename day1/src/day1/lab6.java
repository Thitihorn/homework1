package day1;

public class lab6 {
	public static void main(String[] args) {
		int count = 1;

		while (count <= 10) {
			System.out.print(count + " ");
			count++;
		}
		System.out.println("");

		int[] num1 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		int sumNum = 0;
		for (int i = 0; i < 10; i++) {
			sumNum += num1[i];
			System.out.print(sumNum+ " ");
		}
		System.out.println("");
		
		int[] sum100 = new int[100];
		for(int i = 0; i < sum100.length; i++) {
			sum100[i] = i + 1;
			if (sum100[i] % 12 == 0) {
				System.out.print(sum100[i]+" ");
			}
		}
		System.out.println("");
		
		int myArrays [] = {1,2,3,4,5};
		int countSum = 0;
		for (int counter : myArrays) {
			System.out.print("myArrays Round "+ counter);
			countSum += counter;
			System.out.println(" sum = "+countSum);
		}
		
	}
}
